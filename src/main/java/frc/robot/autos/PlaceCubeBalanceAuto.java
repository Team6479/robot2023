// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.autos;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.WaitUntilCommand;
import frc.robot.subsystems.Claw;
import frc.robot.subsystems.NeoArm;
import frc.robot.subsystems.Swerve;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class PlaceCubeBalanceAuto extends SequentialCommandGroup {
  /** Creates a new PlaceCubeBalanceAuto. */
  public PlaceCubeBalanceAuto(Swerve swerve, NeoArm arm, Claw claw) {
    super(
      new InstantCommand(() -> swerve.zeroGyro()),
      new InstantCommand(() -> claw.setSpeed(0.25)),
      new WaitCommand(0.2),
      new InstantCommand(() -> claw.setSpeed(0)),
      new InstantCommand(() -> swerve.drive(new Translation2d(-0.35, new Rotation2d()), 0, true)),
      new WaitCommand(1),
      new InstantCommand(() -> arm.incrementState()),
      new InstantCommand(() -> arm.incrementState()),
      new InstantCommand(() -> arm.incrementState()),
      new InstantCommand(() -> swerve.toggleBrake()),
      new WaitCommand(3.5),
      new InstantCommand(() -> claw.setSpeed(-0.9)),
      new WaitCommand(0.5),
      new InstantCommand(() -> claw.setSpeed(0)),
      new InstantCommand(() -> arm.decrementState()),
      new InstantCommand(() -> arm.decrementState()),
      new InstantCommand(() -> arm.decrementState()),
      new InstantCommand(() -> swerve.toggleBrake()),
      new InstantCommand(() -> swerve.drive(new Translation2d(-0.35, new Rotation2d()), 0, true)),
      new WaitUntilCommand(() -> Math.abs(swerve.gyro.getRoll()) > 10),
      new InstantCommand(() -> swerve.drive(new Translation2d(-0.3, new Rotation2d()), 0, true)),
      new WaitUntilCommand(() -> Math.abs(swerve.gyro.getRoll()) < 5),
      new InstantCommand(() -> swerve.toggleBrake()),
      new WaitCommand(0.55),
      new InstantCommand(() -> swerve.toggleBrake()),
      new InstantCommand(() -> swerve.drive(new Translation2d(0.2, new Rotation2d()), 0, true)),
      new WaitUntilCommand(() -> Math.abs(swerve.gyro.getRoll()) < 5),
      new InstantCommand(() -> swerve.toggleBrake())
    );
  }
}


// // Copyright (c) FIRST and other WPILib contributors.
// // Open Source Software; you can modify and/or share it under the terms of
// // the WPILib BSD license file in the root directory of this project.

// package frc.robot.autos;

// import edu.wpi.first.math.geometry.Rotation2d;
// import edu.wpi.first.math.geometry.Translation2d;
// import edu.wpi.first.wpilibj2.command.InstantCommand;
// import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
// import edu.wpi.first.wpilibj2.command.WaitCommand;
// import edu.wpi.first.wpilibj2.command.WaitUntilCommand;
// import frc.robot.subsystems.Claw;
// import frc.robot.subsystems.NeoArm;
// import frc.robot.subsystems.Swerve;

// // NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// // information, see:
// // https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
// public class PlaceCubeBalanceAuto extends SequentialCommandGroup {
//   /** Creates a new PlaceCubeBalanceAuto. */
//   public PlaceCubeBalanceAuto(Swerve swerve, NeoArm arm, Claw claw) {
//     super(
//       new InstantCommand(() -> swerve.zeroGyro()),
//       new InstantCommand(() -> claw.setSpeed(0.25)),
//       new WaitCommand(0.2),
//       new InstantCommand(() -> claw.setSpeed(0)),
//       new InstantCommand(() -> swerve.drive(new Translation2d(-0.35, new Rotation2d()), 0, true)),
//       new WaitCommand(1.1),
//       new InstantCommand(() -> swerve.toggleBrake()), // on 
//       new InstantCommand(() -> swerve.toggleBrake()), // off
//       new InstantCommand(() -> arm.incrementState()),
//       new InstantCommand(() -> arm.incrementState()),
//       new InstantCommand(() -> arm.incrementState()),
//       new WaitCommand(3),
//       new InstantCommand(() -> swerve.drive(new Translation2d(0.35, new Rotation2d()), 0, true)),
//       new WaitCommand(.5),
//       new InstantCommand(() -> swerve.toggleBrake()), // on
//       new InstantCommand(() -> swerve.toggleBrake()), // off
//       new InstantCommand(() -> claw.setSpeed(-0.55)),
//       new WaitCommand(0.5),
//       new InstantCommand(() -> claw.setSpeed(0)),
//       new InstantCommand(() -> arm.decrementState()),
//       new InstantCommand(() -> arm.decrementState()),
//       new InstantCommand(() -> arm.decrementState()),
//       new InstantCommand(() -> swerve.drive(new Translation2d(-0.35, new Rotation2d()), 0, true)),
//       new WaitUntilCommand(() -> Math.abs(swerve.gyro.getRoll()) > 10),
//       new InstantCommand(() -> swerve.drive(new Translation2d(-0.3, new Rotation2d()), 0, true)),
//       new WaitUntilCommand(() -> Math.abs(swerve.gyro.getRoll()) < 5),
//       new InstantCommand(() -> swerve.toggleBrake()),
//       new WaitCommand(0.55),
//       new InstantCommand(() -> swerve.toggleBrake()),
//       new InstantCommand(() -> swerve.drive(new Translation2d(0.2, new Rotation2d()), 0, true)),
//       new WaitUntilCommand(() -> Math.abs(swerve.gyro.getRoll()) < 5),
//       new InstantCommand(() -> swerve.toggleBrake())
//     );
//   }
// }
