package frc.robot;

import com.team6479.lib.controllers.CBXboxController;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.PowerDistribution;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.PowerDistribution.ModuleType;
import edu.wpi.first.wpilibj.XboxController.Button;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.autos.BalanceAuto;
import frc.robot.autos.BalanceAutoOverSwitch;
import frc.robot.autos.DriveForward;
import frc.robot.autos.FromLeftBalanceAuto;
import frc.robot.autos.FromRightBalanceAuto;
import frc.robot.autos.PlaceCubeAuto;
import frc.robot.autos.PlaceCubeBalanceAuto;
import frc.robot.commands.IntakePiece;
import frc.robot.commands.TeleopArmPIDTest;
import frc.robot.commands.TeleopArmPositionControl;
import frc.robot.commands.TeleopArmTest;
import frc.robot.commands.TeleopClaw;
import frc.robot.commands.TeleopSwerve;
import frc.robot.subsystems.Claw;
import frc.robot.subsystems.NeoArm;
import frc.robot.subsystems.Swerve;
import frc.robot.subsystems.NeoArm.ArmPosition;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
    /* Controllers */
    private final CBXboxController xbox = new CBXboxController(0);

    /* Drive Controls */
    private final int translationAxis = XboxController.Axis.kLeftY.value;
    private final int strafeAxis = XboxController.Axis.kLeftX.value;
    private final int rotationAxis = XboxController.Axis.kRightX.value;

    /* Driver Buttons */
    private final JoystickButton zeroGyro = new JoystickButton(xbox, XboxController.Button.kY.value);

    /* Subsystems */
    // private final Swerve s_Swerve = new Swerve();

    private final Claw claw = new Claw();
    private final Swerve swerve = new Swerve();

    private final PowerDistribution powerDistribution = new PowerDistribution();


    // the lower limit is much lower than the upper limit because raising the arm decreases the encoder's value.
    // the PID controller is set to enable continous input, so 0 and 360 are treated as the same point
    // 306 is all the way up
    // 55 all the way down
    // moving down increases angle
    // positive speed moves arm up
    private final NeoArm arm = new NeoArm(92, 5, 85, 297);

    private SendableChooser<Command> autonChooser;

    private boolean slowEnabled;


    /** The container for the robot. Contains subsystems, OI devices, and commands. */
    public RobotContainer() {
        autonChooser = new SendableChooser<>();
        slowEnabled = false;
        autonChooser.setDefaultOption("balance don't go out of community", new BalanceAuto(swerve));
        autonChooser.addOption("balance go out of community (over switch)", new BalanceAutoOverSwitch(swerve));
        autonChooser.addOption("balance go around left", new FromLeftBalanceAuto(swerve));
        autonChooser.addOption("balance go around right", new FromRightBalanceAuto(swerve));
        autonChooser.addOption("nothing", new InstantCommand());
        autonChooser.addOption("drive forward 5 seconds", new DriveForward(swerve));
        autonChooser.addOption("place and balance", new PlaceCubeBalanceAuto(swerve, arm, claw));
        autonChooser.addOption("place without balancing", new PlaceCubeAuto(swerve, arm, claw));
        Shuffleboard.getTab("Auto").add(autonChooser);
        swerve.setDefaultCommand(
            new TeleopSwerve(
                swerve, 
                () -> (-xbox.getRawAxis(translationAxis) /** Math.abs(-xbox.getRawAxis(translationAxis)))*/ * (slowEnabled ? 0.5 : 1)) * 0.8, // TODO: change back 
                () -> (-xbox.getRawAxis(strafeAxis) /** Math.abs(-xbox.getRawAxis(strafeAxis))*/ * (slowEnabled ? 0.5 : 1)) * 0.8, 
                () -> -xbox.getRawAxis(rotationAxis) * 0.75 * (slowEnabled ? 0.5 : 1) * 0.6
            )
        );



        // positive moves arm down
        // positive moves wrist towards 0, moving wrist down away from zero is a negative number
        // arm.setDefaultCommand(new TeleopArmTest(arm, () -> xbox.getLeftTriggerAxis(), () -> xbox.getRightTriggerAxis(), () -> xbox.getLeftBumper() ? 0.25 : 0, () -> xbox.getRightBumper() ? 0.25 : 0));

        // arm.setDefaultCommand(new TeleopArmTest2(arm, () -> xbox.getAButton()));

        xbox.getButton(Button.kLeftBumper).onTrue(new InstantCommand(arm::decrementState));
        xbox.getButton(Button.kRightBumper).onTrue(new InstantCommand(arm::incrementState));
        // arm.setDefaultCommand(new TeleopArmPIDTest(arm, () -> xbox.getAButton()));
        arm.setDefaultCommand(new TeleopArmPositionControl(arm));

        xbox.getButton(Button.kB).onTrue(new InstantCommand(swerve::toggleBrake));

        // xbox.getButton(Button.kY).onTrue(new InstantCommand(() -> claw.setSpeed(0.25))).onFalse(new InstantCommand(() -> claw.setSpeed(0)));
        // xbox.getButton(Button.kA).onTrue(new InstantCommand(() -> claw.setSpeed(-0.25))).onFalse(new InstantCommand(() -> claw.setSpeed(0)));

        xbox.getButton(Button.kX).onTrue(new InstantCommand(() -> slowEnabled = !slowEnabled));

        xbox.getButton(Button.kA).onTrue(new InstantCommand(() -> arm.setArmPosition(ArmPosition.SINGLE_LOAD)));

        // arm.setDefaultCommand(new TeleopArmPIDTest(arm, () -> xbox.getAButton()));


        // xbox.getButton(Button.kStart).onTrue(new IntakePiece(claw, powerDistribution));

        claw.setDefaultCommand(new TeleopClaw(claw, () -> xbox.getRightTriggerAxis() - xbox.getLeftTriggerAxis()));

        // xbox.getButton(Button.kRightBumper).onTrue(new PathTest(s_Swerve));

        // CameraServer.startAutomaticCapture();

        // Configure the button bindings
        configureButtonBindings();
    }

    /**
     * Use this method to define your button->command mappings. Buttons can be created by
     * instantiating a {@link GenericHID} or one of its subclasses ({@link
     * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
     * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
     */
    private void configureButtonBindings() {
        /* Driver Buttons */
        zeroGyro.onTrue(new InstantCommand(() -> swerve.zeroGyro()));
    }

    public void teleopPeriodic() {
        // SmartDashboard.putBoolean("pov", xbox.getPOVButton(90, true).getAsBoolean());
    }

    public void teleopInit() {
        if(swerve.isBraking()) swerve.toggleBrake();
    }

    /**
     * Use this to pass the autonomous command to the main {@link Robot} class.
     *
     * @return the command to run in autonomous
     */
    public Command getAutonomousCommand() {
        // An ExampleCommand will run in autonomous
        // return new LeftBlueSideBalance(s_Swerve);
        return autonChooser.getSelected();
        // return new TrajectoryRelative("ramp2.wpilib.json", s_Swerve);
    }
}
