// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.function.BooleanSupplier;

import edu.wpi.first.wpilibj.PowerDistribution;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import frc.robot.subsystems.Claw;

public class IntakePiece extends CommandBase {

  private Claw claw;
  private Timer timer;

  private double previousVoltage;

  private PowerDistribution pdp;

  /** Creates a new ActuateClaw. */
  public IntakePiece(Claw claw, PowerDistribution pdp) {
    this.claw = claw;
    this.timer = new Timer();
    this.pdp = pdp;
    addRequirements(claw);
    this.previousVoltage = 0;
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    timer.reset();
    timer.start();
    this.previousVoltage = claw.getVoltage();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    claw.setSpeed(0.25);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    timer.stop();
    timer.reset();
    claw.setSpeed(0);
  }

  private boolean isVoltageSpike() {
  // pdp will be 12v
  // motor will be 3v or something
  // motor will spike to 5 or something;
    double clawVoltage = claw.getVoltage();
    double pdpVoltage = pdp.getVoltage();
    double requiredSpike = 0.2 * pdpVoltage;
    previousVoltage = claw.getVoltage();
    return clawVoltage <= requiredSpike;
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return timer.get() > 3;
    // return timer.get() > 3 || isVoltageSpike();
  }
}
