// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.NeoArm;

public class TeleopArmTest extends CommandBase {

  private NeoArm arm;

  private DoubleSupplier bigSupplier, smallSupplier, down2, up2;

  private boolean resetting;

  /** Creates a new TeleopArmTest. */
  public TeleopArmTest(NeoArm arm, DoubleSupplier bigSupplier, DoubleSupplier smallSupplier, DoubleSupplier down2, DoubleSupplier up2) {
    this.arm = arm;
    this.bigSupplier = bigSupplier;
    this.smallSupplier = smallSupplier;
    this.down2 = down2;
    this.up2 = up2;
    addRequirements(arm);
    resetting = false;
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    resetting = false;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    arm.setPercentOutputBig(smallSupplier.getAsDouble() - bigSupplier.getAsDouble());
    arm.setPercentOutputSmall(up2.getAsDouble() - down2.getAsDouble());
    // arm.setPercentOutput Small(smallSupplier.getAsDouble());
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
