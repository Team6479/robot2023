// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Transform2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.math.trajectory.TrajectoryUtil;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.SwerveControllerCommand;
import frc.robot.Constants;
import frc.robot.subsystems.Swerve;

public class TrajectoryRelative extends CommandBase {
  
  private SwerveControllerCommand swerveControllerCommand;
  private TrajectoryConfig config;
  private Trajectory exampleTrajectory;
  private ProfiledPIDController thetaController;

  private Swerve s_Swerve;

  private Pose2d initialPose, endPose;
  private List<Translation2d> waypoints;
  private boolean fromPathWeaver = false;
  private boolean resetPose = false;

  /** Creates a new MoveToTarget. */
  public TrajectoryRelative(Pose2d initialPose, List<Translation2d> waypoints, Pose2d endPose, boolean resetPose, Swerve s_Swerve) {
    this.s_Swerve = s_Swerve;
    this.initialPose = initialPose;
    this.endPose = endPose;
    this.waypoints = waypoints;
    this.resetPose = resetPose;
    config =
            new TrajectoryConfig(
                    Constants.AutoConstants.kMaxSpeedMetersPerSecond,
                    Constants.AutoConstants.kMaxAccelerationMetersPerSecondSquared)
                .setKinematics(Constants.Swerve.swerveKinematics);
        // An example trajectory to follow.  All units in meters.
        exampleTrajectory =
            TrajectoryGenerator.generateTrajectory(
                // Since we reset the pose, this line should always be 0,0,0
                initialPose,
                // These are relative to the starting position
                waypoints,
                // Ending position, relative to the robot's initial position, MAKE SURE YOU USE THE RIGHT UNITS
                endPose,
                config);
    // Use addRequirements() here to declare subsystem dependencies.
  }

  public TrajectoryRelative(String trajectoryFilePath, Swerve s_Swerve) {
    this.s_Swerve = s_Swerve;
    Path trajectoryPath = Filesystem.getDeployDirectory().toPath().resolve(trajectoryFilePath);
    fromPathWeaver = true;
    try {
      exampleTrajectory = TrajectoryUtil.fromPathweaverJson(trajectoryPath);
      exampleTrajectory = exampleTrajectory.transformBy(new Transform2d(exampleTrajectory.getInitialPose(), new Pose2d(0,0,new Rotation2d(0))));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    
    try {
      // exampleTrajectory = TrajectoryUtil.fromPathweaverJson(Filesystem.getDeployDirectory().toPath().resolve("funny.wpilib.json"));
      if(resetPose) s_Swerve.resetOdometry(new Pose2d(0, 0, s_Swerve.getPose().getRotation()));

      DriverStation.reportWarning(exampleTrajectory.getStates().get(exampleTrajectory.getStates().size() - 1).poseMeters + "", false);
      thetaController =
            new ProfiledPIDController(
                Constants.AutoConstants.kPThetaController, 0, 0, Constants.AutoConstants.kThetaControllerConstraints);
        thetaController.enableContinuousInput(-Math.PI, Math.PI);

         swerveControllerCommand =
            new SwerveControllerCommand(
                exampleTrajectory,
                s_Swerve::getPose,
                Constants.Swerve.swerveKinematics,
                new PIDController(Constants.AutoConstants.kPXController, 0, 0),
                new PIDController(Constants.AutoConstants.kPYController, 0, 0),
                thetaController,
                s_Swerve::setModuleStates,
                s_Swerve);
      swerveControllerCommand.initialize();
    } catch (Exception e) {
      e.printStackTrace();
    }
    
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    SmartDashboard.putString("current trajectory pos", exampleTrajectory.getStates().get(5).poseMeters.toString());
    swerveControllerCommand.execute();
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    swerveControllerCommand.end(interrupted);
    s_Swerve.drive(
            new Translation2d(0, 0), 
            0,  
            true
        );
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return swerveControllerCommand.isFinished();
  }
}
