// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.NeoArm;
import frc.robot.subsystems.NeoArm.ArmPosition;
import frc.robot.subsystems.NeoArm.MoveMode;

public class TeleopArmPositionControl extends CommandBase {

  private NeoArm arm;

  /** Creates a new TeleopArmPositionControl. */
  public TeleopArmPositionControl(NeoArm arm) {
    this.arm = arm;
    addRequirements(arm);
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    SmartDashboard.putBoolean("small at goal", arm.isSmallAtGoal());
    SmartDashboard.putBoolean("big at goal", arm.isBigAtGoal());
    switch(arm.getCurrentPosition().mode) {
      case BIG_FIRST:
        arm.setBigPosition(arm.getCurrentPosition().big);
        if(arm.isBigAtGoal()) {
          arm.setSmallPosition(arm.getCurrentPosition().small);
        } else {
          arm.setSmallPosition();
        }
        break;
      case SMALL_FIRST:
        arm.setSmallPosition(arm.getCurrentPosition().small);
        if(arm.isSmallAtGoal()) {
          arm.setBigPosition(arm.getCurrentPosition().big);
        } else {
          arm.setBigPosition();
        }
        break;
      case TOGETHER:
        arm.setBigPosition(arm.getCurrentPosition().big);
        arm.setSmallPosition(arm.getCurrentPosition().small);
        break;
      default:
        arm.setBigPosition();
        arm.setSmallPosition();
        break;
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
