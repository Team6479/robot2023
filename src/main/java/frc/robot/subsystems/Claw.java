// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.ClawConstants;

public class Claw extends SubsystemBase {
  /** Creates a new Claw. */

  // private ColorSensorV3 colorSensor;
  // private ColorMatch colorMatcher;
  // public final Color purpleTarget = new Color(0.24, 0.45, 0.30);
  // public final Color yellowTarget = new Color(0.31, 0.52, 0.15);

  private TalonFX clawMotor;

  public Claw() {

    // colorSensor = new ColorSensorV3(Port.kOnboard);
    // colorSensor.configureColorSensor(com.revrobotics.ColorSensorV3.ColorSensorResolution.kColorSensorRes13bit, com.revrobotics.ColorSensorV3.ColorSensorMeasurementRate.kColorRate25ms, GainFactor.kGain18x);
    // colorMatcher = new ColorMatch();
    // colorMatcher.addColorMatch(purpleTarget); 
    // colorMatcher.addColorMatch(yellowTarget);
  

    clawMotor = new TalonFX(ClawConstants.clawMotor);

    clawMotor.setNeutralMode(NeutralMode.Brake);
  }

  public void logColorValues() {
    // SmartDashboard.putNumber("Red", colorSensor.getColor().red);
    // SmartDashboard.putNumber("Green", colorSensor.getColor().green);
    // SmartDashboard.putNumber("Blue", colorSensor.getColor().blue);

    // ColorMatchResult match = colorMatcher.matchClosestColor(colorSensor.getColor());
    // String colorString;

    // if (match.color == purpleTarget) {
    //   colorString = "Purple";
    // } else if (match.color == yellowTarget) {
    //   colorString = "Yellow";
    // } else {
    //   colorString = "Unknown";
    // }

    // SmartDashboard.putString("Color", colorString);
    // SmartDashboard.putNumber("Confidence", match.confidence);
  }

  @Override
  public void periodic() {
    logColorValues();
    SmartDashboard.putNumber("claw voltage", getVoltage());
  }

  public void setSpeed(double speed) {
    clawMotor.set(ControlMode.PercentOutput, speed);
  }

  public double getVoltage() {
    return clawMotor.getMotorOutputVoltage();
  }

  public enum GamePieceColor {
    YELLOW, PURPLE, UNKNOWN
  }
}
