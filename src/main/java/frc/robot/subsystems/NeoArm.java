// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import java.util.ArrayList;

import com.ctre.phoenix.sensors.CANCoder;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.trajectory.TrapezoidProfile.Constraints;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.ArmConstants;

public class NeoArm extends SubsystemBase {

  private CANSparkMax bigArm;
  private CANSparkMax smallArm;

  private CANCoder bigArmEncoder, smallArmEncoder;

  private ProfiledPIDController bigArmPIDController;
  private double bigArmFeedForward;

  private ProfiledPIDController smallArmPIDController;
  private double smallArmFeedforward;

  private SlewRateLimiter bigLimiter, smallLimiter;

  private double bigUpperLimit, smallUpperLimit, bigLowerLimit, smallLowerLimit;

  private ArrayList<ArmPosition> armPositions;
  private int pointer;

  private ArmPosition currentPosition;

  private double bigLastChange, smallLastChange;


  /** Creates a new Arm. */
  // 55 out, 306 in
  public NeoArm(double bigUpperLimit, double bigLowerLimit, double smallUpperLimit, double smallLowerLimit) {

    armPositions = new ArrayList<ArmPosition>();

    armPositions.add(ArmPosition.ZERO);
    armPositions.add(ArmPosition.FLOOR);
    // armPositions.add(ArmPosition.SINGLE_LOAD);
    armPositions.add(ArmPosition.LOAD);
    armPositions.add(ArmPosition.PLACE_3);
    pointer = 0;
    this.currentPosition = armPositions.get(0);

    this.bigUpperLimit = bigUpperLimit;
    this.smallUpperLimit = smallUpperLimit;
    this.bigLowerLimit = bigLowerLimit;
    this.smallLowerLimit = smallLowerLimit;

    this.bigLimiter = new SlewRateLimiter(0.8);
    this.smallLimiter = new SlewRateLimiter(0.8);

    
    bigArmEncoder = new CANCoder(ArmConstants.bigEncoderPort);
    smallArmEncoder = new CANCoder(ArmConstants.smallEncoderPort);

    bigArm = new CANSparkMax(ArmConstants.bigArmPort, MotorType.kBrushless);
    smallArm = new CANSparkMax(ArmConstants.smallArmPort, MotorType.kBrushless);

    bigArmPIDController = new ProfiledPIDController(0.15, 0, 0, new Constraints(80, 60)); //p = 0.15
    bigArmPIDController.reset(getBigPosition());
    bigArmPIDController.setTolerance(5);
    bigArmFeedForward = -0.75; // was -0.6 is -0.75

    bigArmPIDController.enableContinuousInput(0, 360);

    smallArmPIDController = new ProfiledPIDController(0.1, 0, 0.0001, new Constraints(80, 60)); // 180 120
    smallArmPIDController.reset(getSmallPosition());
    smallArmPIDController.setTolerance(10); //TODO: set back to 5 and test
    smallArmFeedforward = -0.361;

    SmartDashboard.putNumber("bigP", bigArmPIDController.getP());
    SmartDashboard.putNumber("bigI", bigArmPIDController.getI());
    SmartDashboard.putNumber("bigD", bigArmPIDController.getD());
    SmartDashboard.putNumber("bigF", bigArmFeedForward);
    SmartDashboard.putBoolean("small cancoder good", true);

    // SmartDashboard.putNumber("smallP", smallArmPIDController.getP());
    // SmartDashboard.putNumber("smallI", smallArmPIDController.getI());
    // SmartDashboard.putNumber("smallD", smallArmPIDController.getD());
    // SmartDashboard.putNumber("smallF", smallArmFeedforward);

    // bigArm.setIdleMode(IdleMode.kCoast);
    // smallArm.setIdleMode(IdleMode.kCoast);

    smallArm.setInverted(true);
    
    smallArm.setIdleMode(IdleMode.kBrake);
    bigArm.setIdleMode(IdleMode.kBrake);

    bigLastChange = getBigPosition();
    smallLastChange = getSmallPosition();
  }

  public void incrementState() {
    if(pointer < armPositions.size()-1) {
      pointer++;
      this.currentPosition = armPositions.get(pointer);
    }
  }

  public void decrementState() {
    if(pointer > 0) {
      pointer--;
      this.currentPosition = armPositions.get(pointer);
    }
  }

  public void setArmPosition(ArmPosition position) {
    this.currentPosition = position;
  }

  public void setBigPosition(double position) {
    // SmartDashboard.putNumber("big input", position);
    // double pid = bigArmPIDController.calculate(getBigPosition(), position);
    // bigArmPIDController.setGoal(position);
    // double out = pid + bigArmFeedForward;
    // SmartDashboard.putNumber("Big controller output", out);
    // SmartDashboard.putNumber("Big setpoint", bigArmPIDController.getSetpoint().position);
    // bigArm.setVoltage(out);
    // bigLastChange = position;
  }

  public void setBigPosition() {
    // SmartDashboard.putNumber("big input", bigLastChange);
    // double pid = bigArmPIDController.calculate(getBigPosition(), bigLastChange);
    // bigArmPIDController.setGoal(bigLastChange);
    // double out = pid + bigArmFeedForward;
    // SmartDashboard.putNumber("Big controller output", out);
    // SmartDashboard.putNumber("Big setpoint", bigArmPIDController.getSetpoint().position);
    // bigArm.setVoltage(out);
  }

  public void setSmallPosition(double position) {
    // if (getSmallPosition() == smallLastChange) {
    //   SmartDashboard.putBoolean("small cancoder good", false);
    //   smallArm.setVoltage(0);
    //   return;
    // } else SmartDashboard.putBoolean("small cancoder good", true);
    
    // double pid = smallArmPIDController.calculate(getSmallPosition(), position);
    // smallArmPIDController.setGoal(position);
    // double out = pid + smallArmFeedforward;
    // SmartDashboard.putNumber("Small controller output", out);
    // SmartDashboard.putNumber("Small setpoint", smallArmPIDController.getSetpoint().position);
    // smallArm.setVoltage(out);
    // smallLastChange = position;
  }

  public void setSmallPosition() {
    // if (getSmallPosition() == smallLastChange) {
    //   SmartDashboard.putBoolean("small cancoder good", false);
    //   smallArm.setVoltage(0);
    //   return;
    // } else SmartDashboard.putBoolean("small cancoder good", true);

    // double pid = smallArmPIDController.calculate(getSmallPosition(), smallLastChange);
    // smallArmPIDController.setGoal(smallLastChange);
    // double out = pid + smallArmFeedforward;
    // SmartDashboard.putNumber("Small controller output", out);
    // SmartDashboard.putNumber("Small setpoint", smallArmPIDController.getSetpoint().position);
    // smallArm.setVoltage(out);
  }

  public void stopBig() {
    bigArm.stopMotor();
  }

  public ArmPosition getCurrentPosition() {
    return currentPosition;
  }

  public void setPercentOutputBig(double speed) {
    // if((getBigPosition() >= bigUpperLimit && speed > 0) || ((getBigPosition() <= bigLowerLimit || getBigPosition() > 200) && speed < 0)) {
    //   bigArm.stopMotor();
    // } else {
    //   bigArm.set(MathUtil.clamp(bigLimiter.calculate(speed), -0.2, 0.2));
    // }
  } 

  // 
  public void setPercentOutputSmall(double speed) {
    // if((getSmallPosition() <= smallLowerLimit && speed > 0 && getSmallPosition() > 250) || (getSmallPosition() >= smallUpperLimit && speed < 0 && getSmallPosition() < 200)) {
    //   smallArm.stopMotor();
    // } else {
    //   smallArm.set(MathUtil.clamp(smallLimiter.calculate(speed), -0.2, 0.2));
    // }
  }

  public void enableSmallContinuousInput(double low, double high) {
    smallArmPIDController.enableContinuousInput(low, high);
  }

  public void disableSmallContinuousInput() {
    smallArmPIDController.disableContinuousInput();
  }

  public boolean isSmallAtGoal() { //  \/ the place the pid controller wants to be     \/ the actual encoder value
    return Math.abs(Math.abs(smallArmPIDController.getGoal().position) - Math.abs(getSmallPosition())) < 5;
  }

  public boolean isBigAtGoal() {
    return Math.abs(Math.abs(bigArmPIDController.getGoal().position) - Math.abs(getBigPosition())) < 5;
  }

  @Override
  public void periodic() {
    SmartDashboard.putNumber("big arm absolute encoder", bigArmEncoder.getAbsolutePosition());
    SmartDashboard.putNumber("small arm absolute encoder", smallArmEncoder.getAbsolutePosition());
    bigArmPIDController.setP(SmartDashboard.getNumber("bigP", 0));
    bigArmPIDController.setI(SmartDashboard.getNumber("bigI", 0));
    bigArmPIDController.setD(SmartDashboard.getNumber("bigD", 0));
    bigArmFeedForward = SmartDashboard.getNumber("bigF", 0);
    // smallArmPIDController.setP(SmartDashboard.getNumber("smallP", 0.05));
    // smallArmPIDController.setI(SmartDashboard.getNumber("smallI", 0));
    // smallArmPIDController.setD(SmartDashboard.getNumber("smallD", 0));
    // smallArmFeedforward = SmartDashboard.getNumber("smallF", 0.1);
    SmartDashboard.putBoolean("ZERO", currentPosition == ArmPosition.ZERO);
    SmartDashboard.putBoolean("FLOOR", currentPosition == ArmPosition.FLOOR);
    SmartDashboard.putBoolean("SINGLE_LOAD", currentPosition == ArmPosition.SINGLE_LOAD);
    SmartDashboard.putBoolean("LOAD", currentPosition == ArmPosition.LOAD);
    SmartDashboard.putBoolean("PLACE_3", currentPosition == ArmPosition.PLACE_3);
    // Shuffleboard.getTab("Arm").addBooleanArray("arm", () -> {
    //   return new boolean[]{
    //     currentPosition == ArmPosition.ZERO,
    //     currentPosition == ArmPosition.FLOOR,
    //     currentPosition == ArmPosition.SINGLE_LOAD,
    //     currentPosition == ArmPosition.LOAD,
    //     currentPosition == ArmPosition.PLACE_3
    //   };
    // });
  }

  // TODO: fix unit conversion (this is not correct)
  public double getBigPosition() {
    return bigArmEncoder.getAbsolutePosition();
    // return Conversions.falconToDegrees(bigArm.getEncoder().getPosition(), bigGearRatio);
  }

  public double getSmallPosition() {
    return smallArmEncoder.getAbsolutePosition();
  }

  public enum ArmPosition {
    ZERO(92, 53, MoveMode.TOGETHER),
    // FLOOR(60, 250, MoveMode.TOGETHER),
    FLOOR(75, 237, MoveMode.TOGETHER),

    SINGLE_LOAD(94, 68, MoveMode.TOGETHER),
    // LOAD(6, 294, MoveMode.TOGETHER),
    LOAD(31, 214, MoveMode.TOGETHER),
    PLACE_3(2, 275, MoveMode.TOGETHER);

    public final double big;
    public final double small;
    public final MoveMode mode;

    private ArmPosition(double big, double small, MoveMode mode) {
      this.big = big;
      this.small = small;
      this.mode = mode;
    }
  }

  public enum MoveMode {
    BIG_FIRST,
    SMALL_FIRST,
    TOGETHER;
  }
}
